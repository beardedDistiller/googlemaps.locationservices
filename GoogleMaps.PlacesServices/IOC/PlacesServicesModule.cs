﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;
using Microsoft.Extensions.DependencyInjection;
namespace GoogleMaps.PlacesServices.IOC
{
    public static class PlacesServicesModule
    {
        public static void AddGooglePlacesServicesModule(this IServiceCollection services, string googlePlacesApiKey)
        {
            services.AddTransient<GooglePlacesService>(s => new GooglePlacesService(googlePlacesApiKey));
        }
    }
}
