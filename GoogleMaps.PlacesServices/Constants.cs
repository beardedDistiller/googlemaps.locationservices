﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GoogleMaps.PlacesServices
{
    public class Constants
    {
        public static class ApiResponses
        {
            public const string ZeroResults = "ZERO_RESULTS";
            public const string OverQueryLimit = "OVER_QUERY_LIMIT";
            public const string RequestDenied = "REQUEST_DENIED";
        }

        public static class ApiUriTemplates
        {
            public const string ApiPlacesLatLongSearch = "maps.googleapis.com/maps/api/place/nearbysearch/json?location={0},{1}&radius={2}";

            public const string ApiPlacesDetailsFromPlaceId = "maps.googleapis.com/maps/api/place/details/json?placeid={0}";

            public const string ApiPlacesFromText= "maps.googleapis.com/maps/api/place/findplacefromtext/json?fields=place_id&inputtype=textquery&input={0}";
        }
    }
}
