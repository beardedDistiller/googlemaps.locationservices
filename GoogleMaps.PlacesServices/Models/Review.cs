﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;
namespace GoogleMaps.PlacesServices.Models
{
    public class Review
    {
        [JsonProperty("author_name")]
        public string AuthorName { get; set; }
        [JsonProperty("author_url")]
        public string AuthorUrl { get; set; }
        [JsonProperty("language")]
        public string Language { get; set; }
        [JsonProperty("profile_photo_url")]
        public string ProfilePhotoUrl { get; set; }
        [JsonProperty("rating")]
        public double Rating { get; set; }
        [JsonProperty("relative_time_description")]
        public string RelativeTimeDescription { get; set; }
        [JsonProperty("text")]
        public string Text { get; set; }
        [JsonProperty("time")]
        public long Time { get; set; }
    }
}
