﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;
namespace GoogleMaps.PlacesServices.Models
{
    public class OpenPeriods
    {
        [JsonProperty("open")]
        public OpenPeriod Open { get; set; }

        [JsonProperty("close")]
        public OpenPeriod Close { get; set; }
    }
}
