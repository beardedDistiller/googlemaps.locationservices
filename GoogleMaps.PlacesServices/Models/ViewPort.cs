﻿using System;
using System.Collections.Generic;
using System.Text;

using Newtonsoft.Json;
namespace GoogleMaps.PlacesServices.Models
{
    public class ViewPort
    {
        [JsonProperty("northeast")]
        public Location NorthEast { get; set; }

        [JsonProperty("southwest")]
        public Location SouthWest { get; set; }
    }
}
