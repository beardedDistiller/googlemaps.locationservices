﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;
namespace GoogleMaps.PlacesServices.Models
{
    internal class PlaceSearchResponse
    {
        public List<string> html_attributions { get; set; }

        [JsonProperty("results")]
        public List<PlaceSearchResult> Results { get; set; }

        [JsonProperty("Status")]
        public string Status { get; set; }

    }

    internal class PlaceFromTextResponse
    {
        public List<string> html_attributions { get; set; }

        [JsonProperty("candidates")]
        public List<PlaceSearchFromTextResult> Candidates { get; set; }

        [JsonProperty("Status")]
        public string Status { get; set; }

    }
}
