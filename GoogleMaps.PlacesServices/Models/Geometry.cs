﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace GoogleMaps.PlacesServices.Models
{
    public class Geometry
    {
        [JsonProperty("location")]
        public Location Location { get; set; }

        [JsonProperty("viewport")]
        public ViewPort ViewPort { get; set; }
    }
}
