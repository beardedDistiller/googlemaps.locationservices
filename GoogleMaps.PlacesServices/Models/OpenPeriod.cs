﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace GoogleMaps.PlacesServices.Models
{
    public class OpenPeriod
    {
        [JsonProperty("day")]
        public int Day { get; set; }

        [JsonProperty("time")]
        public string Time { get; set; }
    }
}
