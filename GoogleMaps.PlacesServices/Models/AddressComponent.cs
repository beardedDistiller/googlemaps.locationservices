﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;
namespace GoogleMaps.PlacesServices.Models
{
    public class AddressComponent
    {
        [JsonProperty("long_name")]
        public string LongName { get; set; }

        [JsonProperty("short_name")]
        public string ShortName { get; set; }

        [JsonProperty("types")]
        public List<string> Types { get; set; }

    }
}
