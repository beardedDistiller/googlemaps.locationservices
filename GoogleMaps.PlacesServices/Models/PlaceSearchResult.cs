﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace GoogleMaps.PlacesServices.Models
{
    public class PlaceSearchResult
    {
        [JsonProperty("geometry")]
        public Geometry Geometry { get; set; }

        [JsonProperty("icon")]
        public string IconUrl { get; set; }

        [JsonProperty("Id")]
        public string Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("opening_hours")]
        public OpeningHours OpeningHours { get; set; }

        [JsonProperty("photos")]
        public List<Photo> Photos { get; set; }

        [JsonProperty("place_id")]
        public string PlaceId { get; set; }


        [JsonProperty("plus_code")]
        public PlusCode PlusCode { get; set; }

        [JsonProperty("reference")]
        public string Reference { get; set; }


        [JsonProperty("rating")]
        public double? Rating { get; set; }


        [JsonProperty("types")]
        public List<string> Types { get; set; }

        [JsonProperty("vicinity")]
        public string Vicinity { get; set; }



        [JsonProperty("scope")]
        public string Scope { get; set; }


        [JsonProperty("user_ratings_total")]
        public int UserRatingsTotal { get; set; }
    }
    public class PlaceSearchFromTextResult
    {
        [JsonProperty("place_id")]
        public string PlaceId { get; set; }

    }
}
