﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;
namespace GoogleMaps.PlacesServices.Models
{
    public class Location
    {
        [JsonProperty("lat")]
        public double Latitude;
        [JsonProperty("lng")]
        public double Longitude;
    }
}
