﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace GoogleMaps.PlacesServices.Models
{
    internal class PlaceDetailsResponse
    {
        public List<string> html_attributions { get; set; }

        [JsonProperty("result")]
        public PlaceDetail Result { get; set; }

        [JsonProperty("Status")]
        public string Status { get; set; }
    }
}
