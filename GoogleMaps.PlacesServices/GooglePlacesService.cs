﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Globalization;
using System.Xml.Linq;
using System.Linq;
using System.Net;
using Newtonsoft.Json;
using GoogleMaps.PlacesServices.Models;
using System.Web;

namespace GoogleMaps.PlacesServices
{
    public class GooglePlacesService
    {


        public GooglePlacesService(string apikey)
        {
            APIKey = apikey;
        }
        private string APIKey { get; set; }

        private string UrlProtocolPrefix = "https://";



        protected string APIUrlPlaceDetailsFromPlaceId
        {
            get
            {
                return UrlProtocolPrefix + Constants.ApiUriTemplates.ApiPlacesDetailsFromPlaceId;
            }
        }

        protected string ApiUrlPlaceDetailsLatLongSearch
        {
            get
            {
                return UrlProtocolPrefix + Constants.ApiUriTemplates.ApiPlacesLatLongSearch;
            }
        }
        protected string ApiUrlPlaceApiPlacesFromText
        {
            get
            {
                return UrlProtocolPrefix + Constants.ApiUriTemplates.ApiPlacesFromText;
            }
        }
        

        /// <summary>
        /// Translates a placeid into a place using Google Maps api
        /// </summary>
        /// <param name="placeId"></param>
        /// <returns></returns>
        public PlaceDetail GetPlaceDetails(string placeId)
        {
            var fullUrl = string.Format(CultureInfo.InvariantCulture, APIUrlPlaceDetailsFromPlaceId, placeId) + "&key=" + APIKey;
            string jsonString;

            using (WebClient wc = new WebClient())
            {
                jsonString = wc.DownloadString(fullUrl);
            }


            var results = JsonConvert.DeserializeObject<PlaceDetailsResponse>(jsonString);


            if (results.Status == Constants.ApiResponses.OverQueryLimit || results.Status == Constants.ApiResponses.RequestDenied)
            {
                throw new System.Net.WebException("Request Not Authorized or Over QueryLimit");
            }

            return results.Result;

        }


        /// <summary>
        /// Translates a Latitude / Longitude into an place using Google Maps api
        /// </summary>
        /// <param name="latitude"></param>
        /// <param name="longitude"></param>
        /// <param name="radius">Radius to search by</param>
        /// <returns></returns>
        public List<PlaceSearchResult> GetPlaceDetailsFromLatLong(double latitude, double longitude, int radius)
        {
            var fullUrl = string.Format(CultureInfo.InvariantCulture, ApiUrlPlaceDetailsLatLongSearch, latitude, longitude, radius) + "&key=" + APIKey;
            string jsonString;

            using (WebClient wc = new WebClient())
            {
                jsonString = wc.DownloadString(fullUrl);
            }


            var results = JsonConvert.DeserializeObject<PlaceSearchResponse>(jsonString);


            if (results.Status == Constants.ApiResponses.OverQueryLimit || results.Status == Constants.ApiResponses.RequestDenied)
            {
                throw new System.Net.WebException("Request Not Authorized or Over QueryLimit");
            }

            return results.Results;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="textQuery"></param>
        /// <returns></returns>
        public List<PlaceSearchFromTextResult> GetPlaceDetailsFromTextQuery(string textQuery)
        {
            var fullUrl = string.Format(CultureInfo.InvariantCulture, ApiUrlPlaceApiPlacesFromText,HttpUtility.UrlEncode(textQuery)) + "&key=" + APIKey;
            string jsonString;

            using (WebClient wc = new WebClient())
            {
                jsonString = wc.DownloadString(fullUrl);
            }


            var results = JsonConvert.DeserializeObject<PlaceFromTextResponse>(jsonString);


            if (results.Status == Constants.ApiResponses.OverQueryLimit || results.Status == Constants.ApiResponses.RequestDenied)
            {
                throw new System.Net.WebException("Request Not Authorized or Over QueryLimit");
            }

            return results.Candidates;
        }
    }
}
