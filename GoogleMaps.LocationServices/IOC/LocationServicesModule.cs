﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;
using Microsoft.Extensions.DependencyInjection;

namespace GoogleMaps.LocationServices.IOC
{
    public static class LocationServicesModule
    {

        public static void AddGoogleLocationServicesModule(this IServiceCollection services, string googleApiKey)
        {
            services.AddTransient<GoogleLocationService>(s => new GoogleLocationService(googleApiKey));
        }
    }
}
